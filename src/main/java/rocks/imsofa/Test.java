package rocks.imsofa;

import org.apache.commons.io.FileUtils;
import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.api.errors.InvalidRemoteException;
import org.eclipse.jgit.api.errors.TransportException;
import org.gitlab4j.api.GitLabApi;
import org.gitlab4j.api.Pager;
import org.gitlab4j.api.models.Group;
import org.gitlab4j.api.models.Project;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

public class Test {
    public static void main(String[] args) {
        try {
            GitLabApi gitLabApi = new GitLabApi("https://gitlab.com/", "glpat-7RpBaQw1pxczYf237vfF");
            Pager<Project> pager = gitLabApi.getProjectApi().getOwnedProjects(1);
            Group group=gitLabApi.getGroupApi().getGroup("codenotecontent");
//            System.out.println(group);
            List<Project> projects=group.getProjects();
            for(Project p : projects){
                System.out.println(p.getHttpUrlToRepo());
//                System.out.println(gitLabApi.getCommitsApi().getCommits(p.getId()));
                System.out.println(p.getName());
                Path localPath = Paths.get("./my_other_test_repo");
                try {
                    FileUtils.deleteDirectory(localPath.toFile());
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
                try (Git git = Git.cloneRepository().setURI(p.getHttpUrlToRepo())
                        .setDirectory(localPath.toFile()).call()) {


                } catch (InvalidRemoteException e) {
                    throw new RuntimeException(e);
                } catch (TransportException e) {
                    throw new RuntimeException(e);
                } catch (GitAPIException e) {
                    throw new RuntimeException(e);
                }
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
