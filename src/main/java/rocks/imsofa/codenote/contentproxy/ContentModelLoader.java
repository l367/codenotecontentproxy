package rocks.imsofa.codenote.contentproxy;

import org.gitlab4j.api.GitLabApi;
import org.gitlab4j.api.GitLabApiException;
import org.gitlab4j.api.models.Group;
import org.gitlab4j.api.models.Project;

import java.util.ArrayList;
import java.util.List;

public class ContentModelLoader {
    public List<ContentModel> findAll(){
        List<ContentModel> ret=new ArrayList<>();
        GitLabApi gitLabApi = new GitLabApi("https://gitlab.com/", "glpat-7RpBaQw1pxczYf237vfF");
        try {
            Group group=gitLabApi.getGroupApi().getGroup("codenotecontent");
//            System.out.println(group);
            List<Project> projects=group.getProjects();
            for(Project p : projects){
                ContentModel contentModel=new ContentModel();
                contentModel.setName(p.getName());
                contentModel.setDescription(p.getDescription());
                contentModel.setRepositoryURL(p.getHttpUrlToRepo());
                ret.add(contentModel);
            }
        } catch (GitLabApiException e) {
            throw new RuntimeException(e);
        }
        return ret;
    }
}
