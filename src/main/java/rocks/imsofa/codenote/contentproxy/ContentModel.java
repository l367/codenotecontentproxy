package rocks.imsofa.codenote.contentproxy;

import org.eclipse.jgit.api.Git;

import java.io.File;

public class ContentModel {
    private String name=null;
    private String description=null;
    private String repositoryURL=null;

    public void clone2File(File folder) throws Exception {
        try (Git git = Git.cloneRepository().setURI(repositoryURL)
                .setDirectory(folder).call()) {
        }
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getRepositoryURL() {
        return repositoryURL;
    }

    public void setRepositoryURL(String repositoryURL) {
        this.repositoryURL = repositoryURL;
    }

    @Override
    public String toString() {
        return "ContentModel{" +
                "name='" + name + '\'' +
                ", repositoryURL='" + repositoryURL + '\'' +
                '}';
    }
}
