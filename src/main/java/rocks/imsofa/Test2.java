package rocks.imsofa;

import org.apache.commons.io.FileUtils;
import rocks.imsofa.codenote.contentproxy.ContentModel;
import rocks.imsofa.codenote.contentproxy.ContentModelLoader;

import java.io.File;
import java.util.List;

public class Test2 {
    public static void main(String[] args) {
        ContentModelLoader loader=new ContentModelLoader();
        List<ContentModel> list=loader.findAll();
        for(ContentModel contentModel : list){
            System.out.println(contentModel);
            File dir=new File(contentModel.getName());
            try {
                FileUtils.deleteDirectory(dir);
                contentModel.clone2File(dir);
            } catch (Exception e) {
                throw new RuntimeException(e);
            }

        }
    }
}
